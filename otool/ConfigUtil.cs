﻿using System;
using System.IO;
using log4net;
using Newtonsoft.Json;

namespace otool
{
    public class ConfigUtil
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SqlExecutor));
        private static readonly ConfigUtil _instance = new ConfigUtil();

        private readonly string _configFile = "config.json";

        private ConfigUtil()
        {
            Config = Read();
        }

        public Config Config { get; set; }

        public static ConfigUtil Instance()
        {
            return _instance;
        }

        private Config Read()
        {
            try
            {
                var fileContent = File.ReadAllText(_configFile);
                var config = JsonConvert.DeserializeObject<Config>(fileContent);
                return config;
            }
            catch (FileNotFoundException e)
            {
                Logger.Error(e.Message);
                throw;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        public void Save()
        {
            Save(Config);
        }

        public void Save(Config config)
        {
            var objectString = JsonConvert.SerializeObject(config, Formatting.Indented);
            File.WriteAllText(_configFile, objectString);
        }
    }
}
