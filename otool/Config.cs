﻿using System.Text;

namespace otool
{
    public class Config
    {
        public string InputDir { get; set; }
        public string OutputDir { get; set; }

        public string DbUrl { get; set; }
        public string DbName { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("InputDir: {0}\n", InputDir);
            sb.AppendFormat("OutputDir: {0}\n", OutputDir);
            sb.AppendFormat("DbUrl: {0}\n", DbUrl);
            sb.AppendFormat("DbName: {0}\n", DbName);
            sb.AppendFormat("User: {0}\n", User);
            sb.AppendFormat("Password: {0}\n", Password);

            return sb.ToString();
        }
    }
}