## 使用说明

- 本程序为c#编写的命令行程序

- 程序启动时会读取同一目录下的配置文件`config.json`，然后根据配置文件的设置，连接`oracle`数据库，从`SQL`脚本目录读取脚本文件并执行，以`execl`文件格式导出查询结果至输出目录


## 配置说明

- InputDir
  存放`SQL`脚本的目录，默认设置为该程序的子目录`sqlScript`

- OutputDir
  存放导出的`excel`文件的目录，默认设置为程序的子目录`excel`

- DbUrl
  连接`oracle`数据库的地址，包含端口

- DbName
  `oracle`数据库名字

- User
  连接`oracle`数据库的用户名

- Password
  连接`oracle`数据库的密码


## 运行

- 程序预置了用于测试的`SQL`脚本，功能是使用`scott/tiger`用户查询一些信息，然后导出。初次运行时需要修改连接的`IP`，并在`oracle`数据库中激活`scott`用户并修改密码为`tiger`。运行时会在控制台输出信息，同时会有日志文件生成并保存在子目录`log`中。

## Log4Net
[config examples](https://logging.apache.org/log4net/release/config-examples.html)
