﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using log4net;

namespace otool
{
    public class ScriptParser
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SqlExecutor));

        private readonly string _filename;
        private readonly List<string> _queries;

        public ScriptParser(string filename)
        {
            _filename = filename;
            _queries = new List<string>();
        }

        public IEnumerable<string> Queries => _queries;

        public void Parse()
        {
            var script = File.ReadAllText(_filename);
            const string pattern = @"[^;]+(\n)?[^;]+;(\s+--.*)?";
            var regex = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var matchCollection = regex.Matches(script);

            var index = 0;
            foreach (Match match in matchCollection)
            {
                char[] trims = {'\r', '\n'};
                var sql = match.Value.Trim(trims).Trim();
                Logger.InfoFormat(" {0} {1}", ++index, sql);
                _queries.Add(sql);
            }
        }
    }
}
