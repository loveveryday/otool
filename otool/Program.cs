﻿using System;
using System.Diagnostics;
using System.IO;
using log4net;

namespace otool
{
    internal static class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SqlExecutor));

        public static void Main(string[] args)
        {
            // start calc time
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            
            Logger.Info("Start " + AppDomain.CurrentDomain.FriendlyName);
//            TestConfigUtil.TestInit();
//            TestConfigUtil.TestWrite();
//            TestDatabase.TestConnect();
//            return;

            try
            {
                var config = ConfigUtil.Instance().Config;
                var db = new DatabaseOracle(config.DbUrl, config.DbName, config.User, config.Password);
                db.Connect();
//                QueryAndSaveToExcel("select * from emp", db, "orcl-emp.xlsx");

                var sqlExecutor = new SqlExecutor(db);
                foreach (var file in Directory.EnumerateFiles(config.InputDir, "*.sql", SearchOption.AllDirectories))
                {
                    var subFilePath = file.Replace(config.InputDir, "").Replace(".sql", "");
                    subFilePath = subFilePath[0] == Path.DirectorySeparatorChar
                        ? subFilePath.Substring(1)
                        : subFilePath;
//                    logger.Info(file);
//                    logger.Info(subFilePath);

                    sqlExecutor.ExecuteScriptFile(file);
                    var sqlExecutorQueryResults = sqlExecutor.QueryResults;

                    for (var i = 0; i < sqlExecutorQueryResults.Count; i++)
                    {
                        var filename = $"{subFilePath}-export-{i}.xlsx";
                        Logger.Info(filename);
                        Office.SaveDataTableToExcel(sqlExecutorQueryResults[i], filename);
                    }
                }

                Logger.InfoFormat("Query count: {0}, failed: {1}", sqlExecutor.Counter.AllQueries,
                    sqlExecutor.Counter.FailedQueries);
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
            }

            // stop calc time
            stopwatch.Stop();
            Logger.InfoFormat("Spent time: {0}", stopwatch.Elapsed);
        }

        private static void QueryAndSaveToExcel(string query, DatabaseOracle oracle, string filename)
        {
            var dataTable = oracle.GetDataTable(query);
            Office.SaveDataTableToExcel(dataTable, filename);
        }
    }
}
