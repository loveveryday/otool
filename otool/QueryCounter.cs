﻿using System;
using System.IO;
using log4net;
using Newtonsoft.Json;

namespace otool
{
    public class QueryCounter
    {
        public int AllQueries { get; set; }
        public int FailedQueries { get; set; }
    }
}