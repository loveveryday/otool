﻿using System;
using System.Data;
using System.IO;
using log4net;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace otool
{
    public static class Office
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SqlExecutor));

        public static void TestNpoiCreate()
        {
            var workbook = new HSSFWorkbook();
            workbook.CreateSheet("Sheet1");
            workbook.CreateSheet("Sheet2");
            workbook.CreateSheet("Sheet3");

            var fileStream = new FileStream("npoi.xls", FileMode.Create);
            workbook.Write(fileStream);
            fileStream.Close();
            workbook.Close();

            // xlsx
            var xssfWorkbook = new XSSFWorkbook();
            xssfWorkbook.CreateSheet("Sheet1");
            xssfWorkbook.CreateSheet("Sheet2");
            xssfWorkbook.CreateSheet("Sheet3");

            var stream = new FileStream("poi.xlsx", FileMode.Create);
            xssfWorkbook.Write(stream);
            stream.Close();
            xssfWorkbook.Close();
        }

        public static void TestNpoiWrite()
        {
            var workbook = new XSSFWorkbook();
            workbook.CreateSheet("Sheet1");

            var sheet = workbook.GetSheet("Sheet1");

            for (var i = 0; i < 10; i++) sheet.CreateRow(i);

            var row = sheet.GetRow(0);
            var cells = new XSSFCell[10];

            for (var i = 0; i < 10; i++) cells[i] = (XSSFCell) row.CreateCell(i);

            cells[0].SetCellValue(true);
            cells[1].SetCellValue(0.01);
            cells[2].SetCellValue("excel");
            cells[3].SetCellValue("be kind, for everyone is fighting a hard battle");

            for (var i = 4; i < 10; i++) cells[i].SetCellValue(i);

            var stream = new FileStream("poi-write.xlsx", FileMode.Create);
            workbook.Write(stream);
            stream.Close();
            workbook.Close();
        }

        public static void TestNpoiRead()
        {
            IWorkbook workbook = null;
            const string filename = "poi-write.xlsx";

            var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);

            if (filename.Contains(".xlsx"))
                workbook = new XSSFWorkbook(fileStream);
            else
                workbook = new HSSFWorkbook(fileStream);

            var sheet = workbook.GetSheetAt(0);
            for (var i = 0; i < sheet.LastRowNum; i++)
            {
                var row = sheet.GetRow(i);
                if (row == null) continue;
                for (var j = 0; j < row.LastCellNum; j++)
                {
                    var s = row.GetCell(j).ToString();
                    Logger.Info(s);
                }
            }

            fileStream.Close();
            workbook.Close();
        }

        private static void SetFont(IWorkbook workbook, ICell cell)
        {
            var cellStyle = workbook.CreateCellStyle();
            var font = workbook.CreateFont();
            font.Color = HSSFColor.Teal.Index;
            font.FontHeight = 16;
            font.Boldweight = (short)FontBoldWeight.Bold;
            cellStyle.SetFont(font);
            cell.CellStyle = cellStyle;
        }

        private static void SetDateFormat(IWorkbook workbook, ICell cell)
        {
            var cellStyle = workbook.CreateCellStyle();
            var dataFormat = workbook.CreateDataFormat();
            cellStyle.DataFormat = dataFormat.GetFormat("yyyy/mm/dd");
            cell.CellStyle = cellStyle;
        }

        private static void SetDecimalFormat(IWorkbook workbook, ICell cell)
        {
            var cellStyle = workbook.CreateCellStyle();
            cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");
            cell.CellStyle = cellStyle;
        }

        private static void SetCellValueByType(IWorkbook workbook, ICell cell, object obj, Type type)
        {
            try
            {
                if (type == typeof(int) || type == typeof(short) || type == typeof(long) || type == typeof(double) || type == typeof(float))
                {
                    cell.SetCellValue(Convert.ToDouble(obj));
                    if (type == typeof(double) || type == typeof(float))
                    {
                        SetDecimalFormat(workbook, cell);
                    }
                }
                else if (type == typeof(string))
                {
                    cell.SetCellValue(obj.ToString());
                }
                else if (type == typeof(DateTime))
                {
                    cell.SetCellValue((DateTime) obj);
                    SetDateFormat(workbook, cell);
                }
                else if (type == typeof(bool))
                {
                    cell.SetCellValue((bool) obj);
                }
                else if (type == typeof(IRichTextString))
                {
                    cell.SetCellValue((IRichTextString) obj);
                }
                else
                {
                    cell.SetCellValue(obj.ToString());
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        public static void SaveDataTableToExcel(DataTable dataTable, string filename)
        {
            var config = ConfigUtil.Instance().Config;

            // full file path
            var filepath = Path.Combine(config.OutputDir, filename);

            // create direcotry if not exist
            var dir = Path.GetDirectoryName(Path.GetFullPath(filepath));
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

            // craete excel object
            var workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet("Sheet1");

            // write columns
            var columns = dataTable.Columns.Count;
            var row = sheet.CreateRow(0);

            for (var i = 0; i < columns; i++)
            {
                var columnName = dataTable.Columns[i].ColumnName;

                var cell = row.CreateCell(i);
                SetFont(workbook, cell);
                cell.SetCellValue(columnName);
            }

            // write rows
            var length = dataTable.Rows.Count;
            for (var i = 0; i < length; i++)
            {
                row = sheet.CreateRow(i + 1);
                var dataTableRow = dataTable.Rows[i];

                for (var j = 0; j < columns; j++)
                {
                    var cell = row.CreateCell(j);
                    var type = dataTable.Rows[i][j].GetType();
                    SetCellValueByType(workbook, cell, dataTable.Rows[i][j], type);
                }
            }

            // set column width
            for (var i = 0; i < columns; i++)
            {
                sheet.AutoSizeColumn(i);
            }

            // save excel
            var stream = new FileStream(filepath, FileMode.Create);
            workbook.Write(stream);
            stream.Close();
            workbook.Close();
        }
    }
}
