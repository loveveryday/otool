﻿using System;
using System.Data;
using log4net;
using Oracle.ManagedDataAccess.Client;

namespace otool
{
    public class DatabaseOracle
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DatabaseOracle));

        private OracleConnection _connection;

        public DatabaseOracle()
        {
            _connection = null;
        }

        public DatabaseOracle(string dbUrl, string dbName, string user, string password)
        {
            DbUrl = dbUrl;
            DbName = dbName;
            User = user;
            Password = password;

            _connection = null;
        }

        public string DbUrl { get; set; }
        public string DbName { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public void Deconstruct()
        {
            _connection?.Close();
        }

        public void Connect()
        {
            var connectString = $"User Id={User};Password={Password};Data Source={DbUrl}/{DbName}";
            Logger.Info(connectString);

            try
            {
                _connection = new OracleConnection(connectString);
                _connection.Open();
            }
            catch (OracleException)
            {
                Logger.Error("Connect to database failed");
                throw;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw;
            }
        }

        public DataTable GetDataTable(string sqlSearch)
        {
            DataTable dataTable = null;
            try
            {
                var dataAdapter = new OracleDataAdapter(sqlSearch, _connection);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
//            dataTable.Dispose();
            }
            catch (Exception e)
            {
                Logger.ErrorFormat("Execute [{0}] failed, reason: {1}", sqlSearch, e.Message);
            }

            return dataTable;
        }

        public int ExecuteNoQuery(string sql)
        {
            var command = _connection.CreateCommand();
            command.CommandText = sql;
            return command.ExecuteNonQuery();
        }
    }
}
