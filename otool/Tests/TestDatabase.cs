﻿namespace otool.Tests
{
    public static class TestDatabase
    {
        public static void TestConnect()
        {
            var config = ConfigUtil.Instance().Config;
            var oracle = new DatabaseOracle(config.DbUrl, config.DbName, config.User, config.Password);
            oracle.Connect();
            Utils.PrintDataTable(oracle.GetDataTable("select * from emp"));
        }
    }
}
