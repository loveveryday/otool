﻿using log4net;

namespace otool.Tests
{
    public static class TestConfigUtil
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SqlExecutor));

        public static void TestInit()
        {
            var config = ConfigUtil.Instance().Config;
            Logger.Info(config.ToString());
        }

        public static void TestWrite()
        {
            var config = new Config
            {
                InputDir = @"/Users/chaos/Desktop/temp/sqlRunner/sqlScripts",
                OutputDir = @"/Users/chaos/Desktop/temp/sqlRunner/excels",

                DbUrl = @"10.1.32.94:1521",
                DbName = @"orcl",
                User = @"scott",
                Password = @"tiger"
            };

            ConfigUtil.Instance().Save(config);
        }
    }
}
