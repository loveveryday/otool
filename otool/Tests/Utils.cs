﻿using System.Data;
using System.Text;
using log4net;

namespace otool.Tests
{
    public static class Utils
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SqlExecutor));

        public static void PrintDataTable(DataTable dataTable)
        {
            var sb = new StringBuilder();
            var columns = dataTable.Columns.Count;
            for (var i = 0; i < columns; i++)
            {
                var dataType = dataTable.Columns[i].DataType;
//                switch (dataType)
//                {
//                        case System.Type.DefaultBinder
//                }
                var type = dataTable.Columns[i].GetType();

                sb.AppendFormat("{0,24}", dataTable.Columns[i].ColumnName + "(" + dataType.Name + ")");
            }

            Logger.Info(sb.ToString());

            var length = dataTable.Rows.Count;
            for (var i = 0; i < length; i++)
            {
                var dataTableRow = dataTable.Rows[i];
                sb.Clear();
                for (var j = 0; j < columns; j++) sb.AppendFormat("{0,24}", dataTable.Rows[i][j]);

                Logger.Info(sb.ToString());
            }
        }
    }
}
