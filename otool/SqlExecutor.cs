﻿using System.Collections.Generic;
using System.Data;
using log4net;

namespace otool
{
    public class SqlExecutor
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SqlExecutor));

        private readonly DatabaseOracle _db;

        public SqlExecutor(DatabaseOracle db)
        {
            _db = db;
            Counter = new QueryCounter();
            QueryResults = new List<DataTable>();
        }

        public QueryCounter Counter { get; }
        public List<DataTable> QueryResults { get; }

        public void ExecuteScriptFile(string filename)
        {
            QueryResults.Clear();

            var scriptParser = new ScriptParser(filename);
            scriptParser.Parse();

            char[] splits = {'\r', '\n', '\t', ' '};
            foreach (var query in scriptParser.Queries)
            {
                var command = query.Split(splits)[0];
                var sql = query.Remove(query.IndexOf(';'));
//                logger.Info("[Execute] [{0}]", sql);
                Logger.InfoFormat("[Execute] [{0}]", sql);

                switch (command.ToLower())
                {
                    case "select":
                        Counter.AllQueries += 1;
                        var dataTable = ExecuteQuery(sql);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            QueryResults.Add(dataTable);
                        }
                        else
                        {
                            Counter.FailedQueries += 1;
                        }
                        break;
                    default:
                        ExecuteNoQuery(query);
                        break;
                }
            }
        }

        private DataTable ExecuteQuery(string sql)
        {
            return _db.GetDataTable(sql);
        }

        private void ExecuteNoQuery(string sql)
        {
            _db.ExecuteNoQuery(sql);
        }
    }
}
